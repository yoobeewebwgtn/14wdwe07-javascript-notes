// Get references to elements
var inputField = document.getElementById('number');
var theButton = document.getElementById('convert');
var gstIncluded = document.getElementById('gst-included');
var gstExcluded = document.getElementById('gst-excluded');
var gstContent = document.getElementById('gst-content');

// Listen to the button for clicks
theButton.onclick = calculate;

// Function to calculate GST
function calculate() {

	// Show result
	gstIncluded.innerHTML = 'GST Included: $'+inputField.value * 1.15;

	// Show result
	gstExcluded.innerHTML = 'GST Excluded: $'+(inputField.value - inputField.value * 3 / 23).toFixed(2);

	// Display result
	gstContent.innerHTML = 'GST Content: $'+(inputField.value * 3 / 23).toFixed(2);

}