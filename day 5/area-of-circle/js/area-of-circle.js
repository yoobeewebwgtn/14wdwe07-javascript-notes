// References of HTML elements
var inputElement = document.getElementById('radius');
var theButton = document.getElementById('calculate');
var resultParagraph = document.getElementById('result');

// Listen to the button for clicks
theButton.onclick = calculate;

// Function to calculate area of a circle
function calculate() {

	var answer = (Math.PI * Math.pow( inputElement.value, 2 )).toFixed(2);

	resultParagraph.innerHTML = 'The area of circle with ' + inputElement.value + ' radius is ' + answer;

}