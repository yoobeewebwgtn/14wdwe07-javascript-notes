// An empty shopping list (array)
var shoppingList = [];

// Get references to HTML elements
var theInput 	= document.getElementById('new-item');
var theButton 	= document.getElementById('add-new-item');
var theListDiv  = document.getElementById('the-list');

// Listen for clicks on the button
theButton.onclick = function() {

	// Make sure the user has actually added an item
	if( theInput.value.trim() == '' ) {
		theListDiv.innerHTML = 'You cannot insert blank items';
		return;
	}

	// Add item to the list
	shoppingList.push( theInput.value );

	// Clear theListDiv so we don't get doubleups
	theListDiv.innerHTML = '';

	// Variable to hold all the formatted HTML list items
	var allTheListItems = '';

	// Loop through every item in the list
	// Wrap the item inside a li tag
	// Put the li inside theListDiv
	for( var i=0; i<shoppingList.length; i++ ) {

		// New list item
		var li = '<li>'+shoppingList[i]+'</li>';

		// Add this to theListDiv
		allTheListItems += li;

	}

	// Close the ul
	theListDiv.innerHTML = '<ul>'+allTheListItems+'</ul>';

}