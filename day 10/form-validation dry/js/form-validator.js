
function validateForm(el) {
	
	// get the form element
	var formElement = document.querySelector(el);

	// find all the form elements inside it
	var inputs = formElement.querySelectorAll('input,select,textarea');

	console.log(el, formElement, inputs.length);

	for(var i = 0; i < inputs.length; i += 1) {
		processField(inputs[i]);
	}

	function processField(field) {
		console.log(field.id, field.classList);
		if ( ! field.id) return;

		var fieldMessage = document.querySelector('#' + field.id + "-message");
		if ( ! fieldMessage) {
			throw new Error("Couldn't find validation error message element [#" + field.id + "-message]");
		}
		fieldMessage.innerHTML = "";
		if (field.classList.contains('v-required')) {
			if (field.type === "checkbox" || field.type === "radio") {
				if ( ! field.checked) {
					fieldMessage.innerHTML = "* Must be checked";
				}
			} else if (field.value == "") {
				fieldMessage.innerHTML = "* Required";
			}
		}
	}
}

console.log("array", [1,2,3])

validateForm("#login-form");
validateForm("#registration-form");