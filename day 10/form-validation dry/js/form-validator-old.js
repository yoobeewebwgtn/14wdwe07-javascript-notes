// Get references to HTML elements
var usernameInput 		= document.getElementById('username');
var usernameMessage 	= document.getElementById('username-message');
var passwordInput 		= document.getElementById('password');
var passwordMessage 	= document.getElementById('password-message');
var loginForm 			= document.getElementById('login-form');

var regUsernameInput 	= document.getElementById('reg-username');
var regUsernameMessage 	= document.getElementById('reg-username-message');
var registrationForm 	= document.getElementById('registration-form');
var regPassword1 		= document.getElementById('reg-password1');
var regPassword1Message = document.getElementById('reg-password1-message');
var regPassword2 		= document.getElementById('reg-password2');
var regPassword2Message = document.getElementById('reg-password2-message');
var regCombinedPasswordMessage = document.getElementById('reg-combined-password-message');
var regAboutMe 			= document.getElementById('reg-about-me');
var regAboutMeMessage	= document.getElementById('reg-about-me-message');

// Listen for the form being submitted
loginForm.onsubmit = function() {

	// Boolean to represent the validation state of the form
	var isValid = true;

	// Trim the username to remove any useless spaces
	var trimmedUsername = usernameInput.value.trim();

	// Validate the username
	if( trimmedUsername == '' ) {
		usernameMessage.innerHTML = '* Required';
		isValid = false;
	} else if( trimmedUsername.length < 5 ) {
		usernameMessage.innerHTML = '* Must be at least 5 characters long';
		isValid = false;
	} else {
		usernameMessage.innerHTML = '';
	}

	// If the password is the same as a blank string
	if( passwordInput.value == '' ) {
		passwordMessage.innerHTML = '* Required';
		isValid = false;
	} else if( passwordInput.value.length < 8 ) {
		passwordMessage.innerHTML = '* Password must be at least 8 characters long';
		isValid = false;
	} else {
		passwordMessage.innerHTML = '';
	}

	// If the isValid is false (form failed validation)
	if( isValid == false ) {
		// Stop the form from submitting
		return false;
	}

}

// When the registration form is submitted
registrationForm.onsubmit = function() {

	// Validate the username
	var trimmedUsername = regUsernameInput.value.trim();

	// Username pattern
	var usernamePattern = /^[a-zA-Z0-9\-_.]{5,30}$/;

	// If Username is the same as a blank string
	if( trimmedUsername == '' ) {
		regUsernameMessage.innerHTML = '* Required';
	} else if( trimmedUsername.length < 5 ) {
		regUsernameMessage.innerHTML = '* Must be at least 5 characters long. You have '+trimmedUsername.length;
	} else if( trimmedUsername.length > 30 ) {
		regUsernameMessage.innerHTML = '* Must be at most 30 characters long. You have '+trimmedUsername.length;
	} else if( usernamePattern.test(trimmedUsername) == false ) {
		regUsernameMessage.innerHTML = '* Only use alphanumeric characters, -, _, and .';
	} else {
		regUsernameMessage.innerHTML = '';
	}

	// Validate the password1
	if( regPassword1.value == '' ) {
		regPassword1Message.innerHTML = '* Required';
	} else if (regPassword1.value.length < 8) {
		regPassword1Message.innerHTML = '* Must be at least 8 characters long. You have '+regPassword1.value.length;
	} else if (regPassword1.value.length > 30) {
		regPassword1Message.innerHTML = '* Must be at most 30 characters long. You have '+regPassword1.value.length;
	} else {
		regPassword1Message.innerHTML = '';
	}

	// Validate the password2
	if( regPassword2.value == '' ) {
		regPassword2Message.innerHTML = '* Required';
	} else if (regPassword2.value.length < 8) {
		regPassword2Message.innerHTML = '* Must be at least 8 characters long. You have '+regPassword2.value.length;
	} else if (regPassword2.value.length > 30) {
		regPassword2Message.innerHTML = '* Must be at most 30 characters long. You have '+regPassword2.value.length;
	} else {
		regPassword2Message.innerHTML = '';
	}

	// Validate the passwords together
	if( regPassword1.value != '' && regPassword2.value != '' ) {
		// Both passwords have been provided
		// Check if passwords match
		if( regPassword1.value != regPassword2.value ) {
			regCombinedPasswordMessage.innerHTML = '* Passwords do not match';
		} else {
			regCombinedPasswordMessage.innerHTML = '';
		}
	}

	// Validate the about me message
	// Pattern for about me message
	var aboutMePattern = /^[\w\-'"\r\s&]{10,1000}$/;

	if( aboutMePattern.test( regAboutMe.value ) == false ) {
		regAboutMeMessage.innerHTML = '* Use of illegal characters';
	} else {
		regAboutMeMessage.innerHTML = '';
	}

	// Stop the form submitting
	return false;

}