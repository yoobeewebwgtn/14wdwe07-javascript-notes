Day 2
=====

## Reading

Eloquent JavaScript, chapters 1 and 2

## Numbers

* integer: a whole number that can be positive or negative. e.g. 5, 2340, -123, 0
* float: "floating point number" a number with some decimal component. 1.23, -12.23, 0.5, -0.000000003

Numbers in JS are either integers or floats. Floats have [issues with accuracy][1]:

```js
0.1 + 0.2; // 0.30000000000000004
```

## `true` and `false`

Boolean values, returned by [comparison operators][2]).

```js
var a = 5;
var b = 7;
a < b;   // true
a > b;   // false
a <= b;  // true
a >= b;  // false
a === b; // false
a !== b; // true
```

## `===` vs `==`

- `==` *loose equality*: values are the same, but not necessarily of the same type 
- `===` *strict equality*: values must be the same type and value 
- `!=` *loose inequality*: values are not the same, even across type
- `!==` *strict inequality*: values do not match, and if they do, their types do not.


```js
"10" == 10;   // true
"10" === 10;  // false 
              // -- strict equality: 
              // must be the same type and value
```

## operator precedence

BEDMAS, but a little more in depth.

[Operator Precedence][3]

## `parseInt`

`parseInt(string, radix)`

The [parseInt()][4] function parses a string argument and returns an integer of the specified radix or base.


```js
var score = "10";
console.log(score); // "10" (string)
console.log(score + 1); // "101" (concatenation)
console.log(score == 10); // true; // loose equality
console.log(score === 10); // false; // strict equality

score = parseInt(score, 10); // 10 means interpret as a base 10 number
console.log(score); // 10 (number)
console.log(score + 1); // 11 (addition)
console.log(score == 10); // true;
console.log(score === 10); // true;
```

## `if` statements

```js
var a = prompt('How many ducks?');
a = parseInt(a, 10);
if (a === 0) {
  console.log('There are no ducks.');
  console.log('I wish there were more.');
} else {
  console.log('I wish I had some bread!');
}
console.log('Bye ducks');
```

## boolean operators

`&&` Boolean AND
`||` Boolean OR
`!`  Boolean NOT

```js
true && true   // --> true
true && false  // --> false 
false && true  // --> false 
false && false // --> false 

true || true   // --> true
true || false  // --> true
false || true  // --> true
false || false // --> false 

!true          // --> false
!false         // --> true

!!true         // --> true
!!false        // --> false
```

## truthy and falsy

[Falsy values][5]

* `false`
* `0` (the number)
* `""` (empty string)
* `null`
* `undefined`
* `NaN` (Not a Number)

Everything else is truthy.

You can use `!!` to determine if truthy or falsy.



[1]: http://floating-point-gui.de/

[2]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Comparison_Operators

[3]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_Precedence

[4]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt

[5]: http://www.sitepoint.com/javascript-truthy-falsy/
