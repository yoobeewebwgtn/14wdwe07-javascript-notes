// Options
var winningNumber = Math.random() * 100000000;
var guesses = 0;
var guess = 0;

// Round the winning number
winningNumber = Math.round(winningNumber);

// Do while
do {
	// Take a guess
	guess = Math.round( Math.random() * 100000000 );

	// Increment the guesses
	guesses++;
} while( guess != winningNumber );

alert('It took ' + guesses + ' guesses to win');