for( var i=1; i<=100; i++ ) {

	// If the iteration is a multiple of 15: FizzBuzz
	if( i % 15 == 0 ) {
		console.log('FizzBuzz');
	}
	// If the iteration is a multiple of 3: Fizz
	else if( i % 3 == 0 ) {
		console.log('Fizz');	
	}
	// If the iteration is a multiple of 5: Buzz
	else if( i % 5 == 0 ) {
		console.log('Buzz');	
	} 
	// Else it's just a normal number: show i
	else {
		console.log(i);
	}
	

}