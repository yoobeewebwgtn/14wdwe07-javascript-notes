// Get a reference to the button
var addButton 		= document.getElementById('add');
var subtractButton 	= document.getElementById('subtract');
var multiplyButton 	= document.getElementById('multiply');
var divideButton 	= document.getElementById('divide');

var input1 			= document.getElementById('num1');
var input2 			= document.getElementById('num2');
var answerParagraph = document.getElementById('answer');

// Add an event listener to the button
addButton.onclick 		= addNumbersTogether;
subtractButton.onclick 	= suctractNumbers;

// Function to add two numbers together
function addNumbersTogether() {
	// Put the answer into the p tag
	answerParagraph.innerHTML = Number(input1.value) + Number(input2.value);
}

function subtractNumbers() {
	// Put the answer into the p tag
	answerParagraph.innerHTML = Number(input1.value) - Number(input2.value);
}