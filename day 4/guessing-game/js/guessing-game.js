// References to elements
var usersGuessInput 	= document.getElementById('users-guess');
var theButton 			= document.getElementById('make-guess');
var messageParagraph 	= document.getElementById('message');
var totalGuessParagraph = document.getElementById('total-guesses');

// Start the game
var winningNumber = Math.round( Math.random() * 10 );
var totalGuesses = 0;

// Listen to the button for clicks
theButton.onclick = play;

// Function that runs when you click on the button
function play() {

	// Increment total amount of guesses
	totalGuesses++;

	// Show the total guesses made
	totalGuessParagraph.innerHTML = 'Total Guesses: '+totalGuesses;

	// Check the guess against the winning number
	if( usersGuessInput.value == winningNumber ) {
		// Won
		messageParagraph.innerHTML = 'You won the game!';
		winningNumber = Math.round( Math.random() * 10 );
		totalGuesses = 0;
	} else if( usersGuessInput.value > winningNumber ) {
		// Greater than
		messageParagraph.innerHTML = 'Too high';
	} else {
		// Less than
		messageParagraph.innerHTML = 'Too low';
	}

}