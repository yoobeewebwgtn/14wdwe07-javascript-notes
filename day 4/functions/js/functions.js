// Function to say hello in the console
function sayHelloInConsole() {
	console.log('Hello from a function');
}

// sayHelloInConsole();
// sayHelloInConsole();
// sayHelloInConsole();

// Function to greet a user
function greetUser( name ) {
	console.log('Hello '+name);
}

// greetUser('Mitchell');
// greetUser('Sam');
// greetUser('Jacob');

// Function to say whether an integer is even or odd
function oddOrEven( numberToCheck ) {

	if( numberToCheck % 2 == 0 ) {
		console.log('Even');
	} else {
		console.log('Odd');
	}

}

// oddOrEven(24);
// oddOrEven(25);
// oddOrEven(26);

// Function to add two numbers together
function addTwoTogether( num1, num2 ) {

	return num1 + num2;

}

// var sum1 = addTwoTogether(15, 25);
// var sum2 = addTwoTogether(25, 5);

// var answer = addTwoTogether( sum1, sum2 );

// alert(answer);

// Function to greet someone who optionally sends their age
function greetUserAgain( name, age ) {
	// Prepare a message for the user
	var message = 'Hello ' + name;
	// Did the user provide an age?
	if( typeof(age) == 'number' ) {
		// Check the age
		if( age >= 18 ) {
			message += ' Here, have a beer!';
		} else {
			message += ' Here, have juice box.'
		}
	}
	alert(message);
}

greetUserAgain('Mike');
greetUserAgain('James', 23);
greetUserAgain('Mitchell', 17);
greetUserAgain('Paul', '17');