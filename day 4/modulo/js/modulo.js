// Check if number is even
if( 23 % 2 == 0 ) {
	console.log('Number is even');
} else {
	console.log('Number is odd');
}

// Checking if multiple of 9
if( 97 % 9 == 0 ) {
	console.log('Multiple of 9');
} else {
	console.log('Not multiple of 9');
}

// Make time out of seconds
var totalSeconds = 10000;
var minutes = totalSeconds / 60 % 60;
var hours = totalSeconds / 3600;
var seconds = totalSeconds % 60;

// Round down the values
minutes = Math.floor(minutes);
hours = Math.floor(hours);
seconds = Math.floor(seconds);

// Display the time
console.log(hours+':'+minutes+':'+seconds);