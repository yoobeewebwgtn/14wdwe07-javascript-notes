JavaScript Notes
================

## Tools
- http://getfirebug.com - Firebug for Firefox

## Textbook Reading
- http://eloquentjavascript.net/ - Introduction and Chapter 1.

## Code

Programs accept _input_ and _process_ it, and produce _output_.


```js
var miles = prompt("Number of miles?");
var km = miles * 1.60934;
alert(km);
```

```js
var fluidounces = prompt("Fluid Oz?");
var millilitres = fluidounces * 29.5735;
alert(millilitres);
```

```js
var hectares = prompt("Hectares?");
var acres = hectares * 2.47105;
alert(acres);
```

```js
var f = prompt("Degrees Fahrenheit?");
var c = ((f - 32) * 5) / 9;
alert (c);
```

check out http://www.mathsisfun.com/temperature-conversion.html

### Exercises

* Input: °C. Output: °F
* Input: Inches. Output: Centimeters
* Coffees cost $3.50 each. Input: Quantity of Coffees. Output: Total Price
* Input: Price excluding GST. Output: Price including GST


## Good code is: 

- *Maintainable* - It can be easily understood and modified.
- *Dependable* - It works the same every time.
- *Efficient* - It works at an appropriate speed.
- *Usable* - It is easy to apply the program to solve a problem.

