// to start validation for <form id="login-form"> ...</form>
// validateForm("#login-form");

// supported classes on input, textarea, select:
// v-required
// v-min-n
// v-max-n
// v-match-id-of-paired-field
// v-email
// v-not-negative
// v-no-html

function validateForm(el) {
	
	// get the form element
	var formElement = document.querySelector(el);

	// find all the form elements inside it
	var inputs = formElement.querySelectorAll('input,select,textarea');

	console.log(el, formElement, inputs.length);

	formElement.addEventListener('submit', function(evt) {
		var thereWasAnError = false;

		for(var i = 0; i < inputs.length; i += 1) {
			var fieldIsValid = processField(inputs[i]);
			if ( ! fieldIsValid) {
				thereWasAnError = true;
			}
		}

		if (thereWasAnError) {
			evt.preventDefault();
		}
	});

	for (var i = 0; i < inputs.length; i += 1) {
		var formInput = inputs[i];
		formInput.addEventListener('blur', function(evt) {
			processField(evt.target);
		});
	}


	function processField(field) {

		console.log(field.id, field.classList);
		if ( ! field.id) return true;

		var fieldMessage = document.querySelector('#' + field.id + "-message");
		if ( ! fieldMessage) {
			throw new Error("Couldn't find validation error message element [#" + field.id + "-message]");
		}

		var fieldIsValid = true;
		fieldMessage.innerHTML = "";

		console.log("CLASSES:", field.className.split(" "));

		var classes = field.className.split(" ");

		classes.forEach(chooseClassValidation);

		return fieldIsValid;


		function chooseClassValidation(theClass) {
			var thisValidationIsValid = true;
			var classFragments = theClass.split("-");
			console.log(classFragments);

			if (classFragments[0] !== "v") return;

			var validationMethod = classFragments[1];

			if (validationMethod == 'required') {
				thisValidationIsValid = validate_required(field, fieldMessage);
			}

			if (validationMethod == 'requiredif') {
				var matchFieldId = classFragments.slice(2).join("-");
				thisValidationIsValid = validate_requiredIf(field, fieldMessage, matchFieldId);
			}

			if (validationMethod == 'min') {
				thisValidationIsValid = validate_min(field, fieldMessage, classFragments[2]);
			}

			if (validationMethod == 'max') {
				thisValidationIsValid = validate_max(field, fieldMessage, classFragments[2]);
			}

			if (validationMethod == 'match') {
				var matchFieldId = classFragments.slice(2).join("-");
				thisValidationIsValid = validate_match(field, fieldMessage, matchFieldId);
			}

			if (validationMethod == 'email') {
				thisValidationIsValid = validate_email(field, fieldMessage);
			}

			if (validationMethod == 'not') {  // not-negative
				thisValidationIsValid = validate_not_negative(field, fieldMessage);
			}

			if (validationMethod == 'no') { // no-html
				thisValidationIsValid = validate_no_html(field, fieldMessage);
			}

			if (! thisValidationIsValid) {
				fieldIsValid = false;
			}

		} // end chooseValidationClass

	} // end processField

	function validate_required(field, fieldMessage) {
		if (field.type === "checkbox" || field.type === "radio") {
			if ( ! field.checked) {
				fieldMessage.innerHTML = "* Must be checked";
				return false;
			}
		} else if (field.value.trim() == "") {
			fieldMessage.innerHTML = "* Required";
			return false;
		}
		return true;
	}

	function validate_requiredIf(field, fieldMessage, matchingFieldId) {
		var matchingField = formElement.querySelector('#' + matchingFieldId);
		if ( ! matchingField) {
			throw new Error("Could not find matching field [#" + matchingFieldId + "]");
		}
		if (matchingField.type === "checkbox" || matchingField.type === "radio") {
			if (matchingField.checked) {
				return validate_required(field, fieldMessage);
			}
		}
	}

	function validate_min(field, fieldMessage, minLength) {
		if (field.value.length === 0) return true;
		if (field.value.trim().length < minLength) {
			fieldMessage.innerHTML = "* Must be at least " + minLength + " characters long.";
			return false;
		}
		return true;
	}

	function validate_max(field, fieldMessage, maxLength) {
		if (field.value.length === 0) return true;
		if (field.value.trim().length > maxLength) {
			fieldMessage.innerHTML = "* Must be less than " + maxLength + " characters long.";
			return false;
		}
		return true;
	}

	function validate_match(field, fieldMessage, matchingFieldId) {
		var matchingField = formElement.querySelector('#' + matchingFieldId);
		if ( ! matchingField) {
			throw new Error("Could not find matching field [#" + matchingFieldId + "]");
		}
		if (field.value !== matchingField.value) {
			fieldMessage.innerHTML = "* This field must match."
			return false;
		}
		return true;
	}
	function validate_email(field, fieldMessage) {
		if (field.value.length === 0) return true;
		var emailAddressPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/;
		if ( ! emailAddressPattern.test(field.value.trim())) {
			fieldMessage.innerHTML = "* Please enter a valid email address.";
			return false;
		}
		return true;
	}

	function validate_not_negative(field, fieldMessage) {
		if (field.value.length === 0) return true;
		var valueNumeric = parseFloat(field.value);
		if (valueNumeric < 0) {
			if (field.tagName.toLowerCase() === "select"){
				fieldMessage.innerHTML = "* Select a option";
			} else {
				fieldMessage.innerHTML = "* Must not be negative";
			}
			return false;
		}
		return true;
	}

	function validate_no_html(field, fieldMessage) {
		if (field.value.length === 0) return true;
		var htmlPattern = /\<\S|\S\>/;
		// a < followed by NOT a space
		// OR
		// a NOT a space followed by > 

		if (htmlPattern.test(field.value.trim())) {
			fieldMessage.innerHTML = "* No HTML tags allowed.";
			return false;
		}
		return true;
	}
}









